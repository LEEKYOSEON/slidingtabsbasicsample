package com.example.android.common.view;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.widget.TextView;

/**
 * Created by VSSCP_KYOSEON on 2016. 11. 24..
 */

public class TrapezoidDrawalble extends Drawable {
	private int mColor;
	private TextView mTextView;
	private int mState;

	@Override
	public void draw(Canvas canvas) {
		int mWidth = mTextView.getWidth();
		int mHeight = mTextView.getHeight();

		Paint paint = new Paint();
		paint.setStrokeWidth(2);
		paint.setColor(mColor);
		paint.setStyle(Paint.Style.FILL);
		paint.setAntiAlias(true);

		Point a;
		Point b;
		if (mState == 1) {
			a = new Point(0, 0);
		} else {
			a = new Point(0 + 100, 0);
		}
		if (mState == 2) {
			b = new Point(mWidth, 0);
		} else {
			b = new Point(mWidth - 100, 0);
		}
		Point c = new Point(mWidth, mHeight);
		Point d = new Point(0, mHeight);

		Path path = new Path();
		path.setFillType(Path.FillType.EVEN_ODD);
		path.moveTo(a.x, a.y);
		path.lineTo(b.x, b.y);
		path.cubicTo(c.x,b.y,b.x,c.y,c.x,c.y);
//		path.lineTo(c.x, c.y);
		path.lineTo(d.x, d.y);
		path.cubicTo(a.x,d.y,d.x,a.y,a.x,a.y);
//		path.lineTo(a.x, a.y);
		path.close();

		canvas.drawPath(path, paint);
	}

	@Override
	public void setAlpha(int alpha) {

	}

	@Override
	public void setColorFilter(ColorFilter colorFilter) {

	}

	@Override
	public int getOpacity() {
		return 0;
	}

	public void setCoordinateValue(int color, TextView textView, int state) {
		mColor = color;
		mTextView = textView;
		mState = state;
	}
}
