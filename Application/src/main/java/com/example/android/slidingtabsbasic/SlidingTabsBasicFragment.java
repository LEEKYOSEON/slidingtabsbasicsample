/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.slidingtabsbasic;

import com.example.android.common.logger.Log;
import com.example.android.common.view.SlidingTabLayout;
import com.example.android.common.view.TrapezoidDrawalble;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * A basic sample which shows how to use {@link com.example.android.common.view.SlidingTabLayout}
 * to display a custom {@link ViewPager} title strip which gives continuous feedback to the user
 * when scrolling.
 */
public class SlidingTabsBasicFragment extends Fragment {

    static final String LOG_TAG = "SlidingTabsBasicFragment";

    /**
     * A custom {@link ViewPager} title strip which looks much like Tabs present in Android v4.0 and
     * above, but is designed to give continuous feedback to the user when scrolling.
     */
    private SlidingTabLayout mSlidingTabLayout;

    /**
     * A {@link ViewPager} which will be used in conjunction with the {@link SlidingTabLayout} above.
     */
    private ViewPager mViewPager;
    private SamplePagerAdapter mPagerAdapter;
    private int mPrePosition = -1;

    /**
     * Inflates the {@link View} which will be displayed by this {@link Fragment}, from the app's
     * resources.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sample, container, false);
    }

    // BEGIN_INCLUDE (fragment_onviewcreated)
    /**
     * This is called after the {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} has finished.
     * Here we can pick out the {@link View}s we need to configure from the content view.
     *
     * We set the {@link ViewPager}'s adapter to be an instance of {@link SamplePagerAdapter}. The
     * {@link SlidingTabLayout} is then given the {@link ViewPager} so that it can populate itself.
     *
     * @param view View created in {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // BEGIN_INCLUDE (setup_viewpager)
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mPagerAdapter = new SamplePagerAdapter();
        mPagerAdapter.setCount(1);
        mViewPager.setAdapter(mPagerAdapter);
        // END_INCLUDE (setup_viewpager)

//        ImageButton mAddButton = (ImageButton) view.findViewById(R.id.btn_plus);
        TextView mAddButton = (TextView)view.findViewById(R.id.btn_plus);
        int tabWidth = getResources().getDisplayMetrics().widthPixels * 1 / 3;
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(tabWidth, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.END);
        mAddButton.setLayoutParams(params);
        mAddButton.setPadding(0,10,0,10);
        TrapezoidDrawalble mTrapezoidDrawalble = new TrapezoidDrawalble();
        mTrapezoidDrawalble.setCoordinateValue(getResources().getColor(R.color.tabplus_color),mAddButton,2);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mAddButton.setBackground(mTrapezoidDrawalble);
        }
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPagerAdapter.setCount(mPagerAdapter.getCount()+1);
                mPagerAdapter.notifyDataSetChanged();
                mViewPager.setCurrentItem(mPagerAdapter.getCount());
//                mSlidingTabLayout.setViewPager(mViewPager,mPagerAdapter.getCount()-1);
            }
        });

        // BEGIN_INCLUDE (setup_slidingtablayout)
        // Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
        // it's PagerAdapter set.
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager,0);
        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.i(LOG_TAG,"onPageScrolled : position = "+position);
            }

            @Override
            public void onPageSelected(int position) {
                Log.i(LOG_TAG,"onPageSelected : position = "+position);
                mSlidingTabLayout.setViewPager(mViewPager,position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.i(LOG_TAG,"onPageScrollStateChanged : state = "+state);
            }
        });
        // END_INCLUDE (setup_slidingtablayout)

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mSlidingTabLayout.getVisibility() == View.GONE) {
//                    Log.i(LOG_TAG, "sliding Tab gone");
                    return true;
                } else {
//                    Log.i(LOG_TAG, "sliding Tab visible");
                    return false;
                }
            }
        });
    }
    // END_INCLUDE (fragment_onviewcreated)

    /**
     * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
     * The individual pages are simple and just display two lines of text. The important section of
     * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
     * {@link SlidingTabLayout}.
     */
    public class SamplePagerAdapter extends PagerAdapter {

        private int[] drawablesIds = {
                R.drawable.red_tab,
                R.drawable.blue_tab,
                R.drawable.yellow_tab
        };

        private int[] colorIds = {
                Color.RED,
                Color.BLUE,
                Color.GREEN
        };

        private int mTabCount;

        /**
         * @return the number of pages to display
         */
        @Override
        public int getCount() {
            return mTabCount;
        }

        public void setCount(int mTabCount) {
            this.mTabCount = mTabCount;
        }

        /**
         * @return true if the value returned from {@link #instantiateItem(ViewGroup, int)} is the
         * same object as the {@link View} added to the {@link ViewPager}.
         */
        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        // BEGIN_INCLUDE (pageradapter_getpagetitle)
        /**
         * Return the title of the item at {@code position}. This is important as what this method
         * returns is what is displayed in the {@link SlidingTabLayout}.
         * <p>
         * Here we construct one using the position value, but for real application the title should
         * refer to the item's contents.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            return "Item " + (position + 1);
        }
        // END_INCLUDE (pageradapter_getpagetitle)

        public int getDrawableId(int position){
            //Here is only example for getting tab drawables
            return drawablesIds[position];
        }

        /**
         * Instantiate the {@link View} which should be displayed at {@code position}. Here we
         * inflate a layout from the apps resources and then change the text view to signify the position.
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // Inflate a new layout from our resources

//            if (position == 2) {
//                return new BlankFragment();
//            }
            View view = getActivity().getLayoutInflater().inflate(R.layout.pager_item,
                    container, false);
            // Add the newly created View to the ViewPager
            container.addView(view);

            // Retrieve a TextView from the inflated View, and update it's text
            TextView title = (TextView) view.findViewById(R.id.item_title);
            title.setText(String.valueOf(position + 1));
            view.setBackgroundColor(colorIds[position % 3]);

            Button tabbutton = (Button)view.findViewById(R.id.btn_tab);
            tabbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSlidingTabLayout.isShown()) {
                        mSlidingTabLayout.setVisibility(View.GONE);
                    } else {
                        mSlidingTabLayout.setVisibility(View.VISIBLE);
                    }
                }
            });

            Log.i(LOG_TAG, "instantiateItem() [position: " + position + "]");

            // Return the View
            return view;
        }

        /**
         * Destroy the item from the {@link ViewPager}. In our case this is simply removing the
         * {@link View}.
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
            Log.i(LOG_TAG, "destroyItem() [position: " + position + "]");
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
//            return super.getItemPosition(object);
        }
    }
}
